/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author user050
 */
public class LigneCommande {
    private Produit leproduit;
    private int quantite;
    private float montant;

    public LigneCommande(Produit leproduit, int quantite) {
        this.leproduit = leproduit;
        this.quantite = quantite;
        montant = quantite * leproduit.getPrix();
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Produit getLeproduit() {
        return leproduit;
    }

    public void setLeproduit(Produit leproduit) {
        this.leproduit = leproduit;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }
    
    public String getLibelle(){
        return leproduit.getLibelle();
    }
    
    public String getPrix(){
        return leproduit.getPrix() + " €";
    }
    
    public int getStock(){
        return leproduit.getQuantiteEnStock();
    }
    
}
