/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user050
 */
public class Commande {
    private float montant = 0;
    private List<LigneCommande> lesLigneCommandes;
    
    public void ajouterProduit(Produit produit, int quantite){
        LigneCommande uneLigne = new LigneCommande(produit, quantite);
        lesLigneCommandes.add(uneLigne);
        montant += uneLigne.getMontant();
        produit.retirerDuStock(quantite);
    }
    
    public static Commande creerPanier(){
        Commande laCommande = new Commande();
        laCommande.lesLigneCommandes = new ArrayList<>(5);
        return laCommande;
    }

    public float getMontant() {
        return montant;
    }

    public List<LigneCommande> getLesLigneCommandes() {
        return lesLigneCommandes;
    }
    
    
    
}
