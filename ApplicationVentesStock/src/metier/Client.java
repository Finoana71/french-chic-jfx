/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user050
 */
public class Client {

    public static List<Client> getLesClients() {
        return lesClients;
    }

    public static void setLesClients(List<Client> aLesClients) {
        lesClients = aLesClients;
    }
    private String nom;
    private String prenom;
    private String pseudo;
    private String motDePasse;
    private String numero;
    
    private static List<Client> lesClients;
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Client(String nom, String prenom, String pseudo, String motDePasse) {
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.motDePasse = motDePasse;
    }
    
    public boolean estValidePseudoMotDePasse(String pseudo, String mdp){
        return this.pseudo.compareTo(pseudo) == 0 && this.motDePasse.compareTo(mdp) == 0;
    }
    
    public static void initializeClients(){
        lesClients = new ArrayList<Client>(3);
        lesClients.add(new Client("Dupond", "Marie", "dupond50", "1234"));
        lesClients.add(new Client("Finoana", "Mandresy", "finoana01", "1234"));
        lesClients.add(new Client("Harilanto", "Danih", "danih12", "1234"));
    }
    
    public static Client rechercherClientParPseudo(String pseudo, String motDePasse){
        for(int i = 0; i < lesClients.size(); i++){
            if(lesClients.get(i).estValidePseudoMotDePasse(pseudo, motDePasse))
                return lesClients.get(i);
        }
        return null;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    
}
