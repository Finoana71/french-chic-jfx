/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user050
 */
public class Produit {

    public static List<Produit> getLesProduits() {
        return lesProduits;
    }

    public static void setLesProduits(List<Produit> aLesProduits) {
        lesProduits = aLesProduits;
    }
    private String reference;
    private String libelle;
    private float prix;
    private boolean estDuJour;
    private int quantiteEnStock;
    
    private static List<Produit> lesProduits;
    
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    
//    List<Produit>
    
    public boolean isEstDuJour() {
        return estDuJour;
    }

    public void setEstDuJour(boolean estDuJour) {
        this.estDuJour = estDuJour;
    }

    public int getQuantiteEnStock() {
        return quantiteEnStock;
    }

    public void setQuantiteEnStock(int quantiteEnStock) {
        this.quantiteEnStock = quantiteEnStock;
    }

    public Produit(String libelle, float prix, boolean estDuJour, int quantiteEnStock) {
        this.libelle = libelle;
        this.prix = prix;
        this.estDuJour = estDuJour;
        this.quantiteEnStock = quantiteEnStock;
    }
    
    public static void initializeProduits(){
        lesProduits = new ArrayList<Produit>(3);
        lesProduits.add(new Produit("pantalon zouk", 50, true, 34));
        lesProduits.add(new Produit("sac", 30, false, 35));
        lesProduits.add(new Produit("chemise zouk", 100, false, 36));
    }

    public static Produit rechercherProduitDuJour(){
        for(int i = 0; i < lesProduits.size(); i++){
            if(lesProduits.get(i).estDuJour)
                return lesProduits.get(i); 
        }
        return null;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
    
    public void retirerDuStock(int quantite){
        quantiteEnStock -= quantite;
    }
}
