/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import controleur.Session;
import controleur.reponse.TraiterConnexionReponse;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import metier.Client;
import metier.Produit;

/**
 *
 * @author user050
 */
public class ApplicationVentesStock extends Application {
    
    public static Session laSession;
    
    private void afficherEcranAccueil(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("vue/ecran/EcranAccueil.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        laSession = new Session();
        TraiterConnexionReponse reponse = laSession.traiterConnexion();
        if (reponse.typeEcran == Session.EnumTypeEcran.ECRAN_ACCUEIL) {
            afficherEcranAccueil(stage);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        initialize();
        launch(args);
    }
    
        
    private static void initialize(){
        Client.initializeClients();
        Produit.initializeProduits();
    }
    
}
