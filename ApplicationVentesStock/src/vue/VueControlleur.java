/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import controleur.Session;
import controleur.reponse.TraiterAjoutPanierReponse;
import controleur.reponse.TraiterIdentificationReponse;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import metier.Client;
import metier.Commande;
import metier.LigneCommande;
import metier.Produit;

/**
 *
 * @author user050
 */
public class VueControlleur implements Initializable {
    
    
    @FXML
    private TextField pseudoField;

    @FXML
    private Button identifierBtn;

    @FXML
    private TextField mdpField;
    
    @FXML
    private TextField quantiteField;
    
    static Produit produitDuJour;
    
    @FXML
    private void handleIdentifierAction(ActionEvent event) {
        TraiterIdentificationReponse reponse = ApplicationVentesStock.laSession.traiterIdentification(pseudoField.getText(), mdpField.getText());;
        if (reponse.typeEcran == Session.EnumTypeEcran.ECRAN_ACCUEIL_PERSO) {
            afficherEcranAccueilPerso(reponse.leClient, reponse.leProduit);
        }
    }
    
    @FXML
    private void handleAjouterPanierAction(ActionEvent event) {
        int quantite = Integer.parseInt(quantiteField.getText());
        TraiterAjoutPanierReponse reponse = ApplicationVentesStock.laSession.traiterAjoutProduitPanier(produitDuJour, quantite);
        
        if (reponse.typeEcran == Session.EnumTypeEcran.ECRAN_PANIER) {
            afficherEcranPanier(reponse.laCommande);
        }

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }    
 
    private void afficherEcranAccueilPerso(final Client client, final Produit produit) {
         try {
            
            produitDuJour = produit;
            String bonjourTxt = "Bonjour " + client.getPrenom() + " " + client.getNom();
            String produitTxt = "Le produit du jour est le \"" + produit.getLibelle() + "\" au prix de " + produit.getPrix() + " Euros";
            
            Scene scene = identifierBtn.getScene();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("vue/ecran/EcranAccueilPerso.fxml"));
            AnchorPane ap = fxmlLoader.load();
            
            Label bonjourLbl = (Label)fxmlLoader.getNamespace().get("bonjourLbl");
            Label produitJourLbl = (Label)fxmlLoader.getNamespace().get("produitJourLbl");
            
            bonjourLbl.setText(bonjourTxt);
            produitJourLbl.setText(produitTxt);
            
            scene.setRoot(ap);
            
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void afficherEcranPanier(Commande laCommande) {
         try {
            
            Scene scene = quantiteField.getScene();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("vue/ecran/EcranPanier.fxml"));
            AnchorPane ap = fxmlLoader.load();
            
            Label montantPanierField = (Label)fxmlLoader.getNamespace().get("montantPanierField");
            montantPanierField.setText(laCommande.getMontant() + " Euros");
            
            TableView panierTable = (TableView)fxmlLoader.getNamespace().get("panierTable");
            setColonneTablePanier(panierTable);
            panierTable.getItems().add(laCommande.getLesLigneCommandes().get(0));

            scene.setRoot(ap);
            
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
    
    private void setColonneTablePanier(TableView table){

      TableColumn<LigneCommande, String> montantCol = (TableColumn)table.getColumns().get(3);
      TableColumn<LigneCommande, String> libelleCol = (TableColumn)table.getColumns().get(0);
      TableColumn<LigneCommande, String> prixCol = (TableColumn)table.getColumns().get(1);
      TableColumn<LigneCommande, String> quantiteCol = (TableColumn)table.getColumns().get(2);
      TableColumn<LigneCommande, String> stockCol = (TableColumn)table.getColumns().get(4);

      montantCol.setCellValueFactory(new PropertyValueFactory<>("montant"));
      libelleCol.setCellValueFactory(new PropertyValueFactory<>("libelle"));
      prixCol.setCellValueFactory(new PropertyValueFactory<>("prix"));
      stockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
      quantiteCol.setCellValueFactory(new PropertyValueFactory<>("quantite"));
    }
}
