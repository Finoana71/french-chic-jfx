/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import controleur.reponse.TraiterAjoutPanierReponse;
import controleur.reponse.TraiterConnexionReponse;
import controleur.reponse.TraiterIdentificationReponse;
import metier.Client;
import metier.Commande;
import metier.Produit;

/**
 *
 * @author user050
 */
public class Session {
    EnumTypeEcran ecranCourant;
    public enum EnumTypeEcran{
        ECRAN_ACCUEIL,
        ECRAN_ACCUEIL_PERSO,
        ECRAN_PANIER
    }
    
    public TraiterConnexionReponse traiterConnexion(){
        ecranCourant = EnumTypeEcran.ECRAN_ACCUEIL; 
        TraiterConnexionReponse reponse = new TraiterConnexionReponse(ecranCourant);
        return reponse;
    }
    
    public Session(){
        super();
    }
    
    public TraiterIdentificationReponse traiterIdentification(String pseudo, String motDePasse){
        ecranCourant = EnumTypeEcran.ECRAN_ACCUEIL_PERSO; 
        Client leClient = Client.rechercherClientParPseudo(pseudo, motDePasse);
        Produit leProduit = Produit.rechercherProduitDuJour();
        TraiterIdentificationReponse reponse = new TraiterIdentificationReponse(ecranCourant, leClient, leProduit);
        return reponse;
    }
    
    public TraiterAjoutPanierReponse traiterAjoutProduitPanier(Produit leproduit, int quantite){
        ecranCourant = EnumTypeEcran.ECRAN_PANIER; 
        Commande laCommande = Commande.creerPanier().creerPanier();
        laCommande.ajouterProduit(leproduit, quantite);
        TraiterAjoutPanierReponse reponse = new TraiterAjoutPanierReponse(ecranCourant, laCommande);
        return reponse;
    }
}

