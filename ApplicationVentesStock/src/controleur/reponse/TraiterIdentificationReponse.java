/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur.reponse;
import controleur.Session.EnumTypeEcran;
import metier.Client;
import metier.Produit;

/**
 *
 * @author user050
 */
public class TraiterIdentificationReponse {
    public EnumTypeEcran typeEcran;
    public Client leClient;
    public Produit leProduit;

    public TraiterIdentificationReponse(EnumTypeEcran typeEcran, Client leClient, Produit leProduit) {
        this.typeEcran = typeEcran;
        this.leClient = leClient;
        this.leProduit = leProduit;
    }
    
    
}
