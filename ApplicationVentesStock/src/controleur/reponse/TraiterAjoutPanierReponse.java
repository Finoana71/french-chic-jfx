/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur.reponse;

import controleur.Session;
import metier.Commande;

/**
 *
 * @author user050
 */
public class TraiterAjoutPanierReponse {
    public Session.EnumTypeEcran typeEcran;
    public Commande laCommande;
    public TraiterAjoutPanierReponse(Session.EnumTypeEcran typeEcran, Commande laCommande) {
        this.typeEcran = typeEcran;
        this.laCommande = laCommande;
    }
}
